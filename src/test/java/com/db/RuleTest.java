package com.db;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * RuleTest
 */
public class RuleTest {
	KieSession ksession;
    private static KieContainer kieContainer = null;
    
    public static void main(String[] args) {
        RuleTest ruleTest = new RuleTest();
        ruleTest.test();     
    }

    public void test() {
        ksession = getKieContainer().newKieSession();

        SurveillanceEvent se1 = new SurveillanceEvent(1L,"accountA", 100.0, "bk1");
        SurveillanceEvent se2 = new SurveillanceEvent(2L,"accountA", 120.0, "bk1");

        ksession.insert(se1);
        ksession.insert(se2);
        ksession.insert( new SurveillanceEvent(3L,"accountB", 10000.0, "bk2") );

        ksession.getAgenda().getAgendaGroup("events").setFocus();
        ksession.fireAllRules();

    }
    
    private static synchronized KieContainer getKieContainer() {
		if (kieContainer == null) {
			KieServices kieServices = KieServices.Factory.get();
			kieContainer = kieServices.getKieClasspathContainer();
		}

		return kieContainer;
	}
}