package com.db;

import java.io.Serializable;

public class SurveillanceEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long transactionId;
    private String account;
    private Double amount;
    private String source;

    public SurveillanceEvent() {
    }
    
    /**
     * @param transactionId
     * @param account
     * @param amount
     * @param source
     */
    public SurveillanceEvent(Long transactionId, String account, Double amount, String source) {
        this.transactionId = transactionId;
        this.account = account;
        this.amount = amount;
        this.source = source;
    }

    @Override
    public String toString() {
        return String.format("SurveillanceEvent {transactionId: %s, account: %s, amount: %s, source: %s}",transactionId, account,
                amount, source);
    }    

    //**************************
    // GETTERS / SETTERS
    //**************************

    /**
     * @return the transactionId
     */
    public Long getTransactionId() {
        return transactionId;
    }

    /**
     * @param transactionId the transactionId to set
     */
    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * @return the account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(String source) {
        this.source = source;
    }
}