package com.db;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

import org.jbpm.casemgmt.api.CaseService;
import org.jbpm.casemgmt.api.model.instance.CaseFileInstance;
import org.jbpm.services.api.service.ServiceRegistry;
import org.kie.api.runtime.KieRuntime;
import org.kie.api.runtime.process.CaseAssignment;
import org.kie.api.runtime.process.ProcessContext;
import org.kie.api.task.model.OrganizationalEntity;

/**
 * Script
 */
public class Script {
    public static void initRoles(ProcessContext kcontext) {
        CaseAssignment caseAssignment = kcontext.getCaseAssignment();

        Collection<OrganizationalEntity> entities = caseAssignment.getAssignments("owner");
        OrganizationalEntity owner = entities.iterator()
                                             .next();

        CaseService caseService = (CaseService) ServiceRegistry.get()
                                                               .service(ServiceRegistry.CASE_SERVICE);
        String caseId = ((CaseFileInstance)kcontext.getCaseData()).getCaseId();
        caseService.assignToCaseRole(caseId, "consult", owner);
        caseService.assignToCaseRole(caseId, "business", owner);

    }

    public static void retrieveUsers(ProcessContext kcontext) {
        Collection<OrganizationalEntity> assignments = kcontext.getCaseAssignment()
                                                               .getAssignments("business");
        Collection<String> users = assignments.stream()
                                              .map(a -> a.getId())
                                              .collect(Collectors.toList());
        kcontext.setVariable("users", users);
    }

    public static void loadEvents(ProcessContext kcontext) {
        EventGroup events = new EventGroup();
        events.add(new SurveillanceEvent(1L, "accountA", 1000.0, "bk1"));
        events.add(new SurveillanceEvent(2L, "accountA", 1200.0, "bk1"));
        events.add(new SurveillanceEvent(3L, "accountB", 1000.0, "bk2"));
        events.add(new SurveillanceEvent(4L, "accountC", 10000.0, "bk2"));
        events.add(new SurveillanceEvent(5L, "accountD", 1200.0, "bk1"));

        kcontext.setVariable("events", events);
    }

    public static void insertWM(ProcessContext kcontext) {
        EventGroup events = (EventGroup) kcontext.getVariable("events");

        KieRuntime kieRuntime = kcontext.getKieRuntime();

        for (SurveillanceEvent event : events.getList()) {
            kieRuntime.insert(event);
        }
    }

    public static void debug(ProcessContext kcontext) {
        Map<String, Object> data = kcontext.getCaseData()
                                           .getData();

        for (Map.Entry<?, ?> element : data.entrySet()) {
            System.out.format("key: %s, value: %s\n", element.getKey(), element.getValue());
        }

        for (Object object : kcontext.getKieRuntime()
                                     .getObjects()) {
            System.out.format("session obj: %s\n", object);
        }

        System.out.format("users: %s\n", kcontext.getVariable("users"));
    }
}