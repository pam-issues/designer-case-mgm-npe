package com.db;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EventGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    private String feature;
    private List<SurveillanceEvent> list = new ArrayList<>();

    public EventGroup() {
    }
    
    /**
     * @param feature
     */
    public EventGroup(String feature) {
        this.feature = feature;
    }

    public void add(SurveillanceEvent event) {
        list.add(event);
    }

    @Override
    public String toString() {
        return String.format("EventGroup {feature: %s, list: %s }", feature, list);
    }

    /**
     * @return the feature
     */
    public String getFeature() {
        return feature;
    }

    /**
     * @param feature the feature to set
     */
    public void setFeature(String feature) {
        this.feature = feature;
    }

    /**
     * @return the list
     */
    public List<SurveillanceEvent> getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(List<SurveillanceEvent> list) {
        this.list = list;
    }
}