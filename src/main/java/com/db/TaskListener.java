package com.db;

import java.util.HashMap;
import java.util.Map;

import org.jbpm.casemgmt.api.CaseService;
import org.jbpm.casemgmt.api.model.instance.CaseFileInstance;
import org.jbpm.services.api.RuntimeDataService;
import org.jbpm.services.api.model.ProcessInstanceDesc;
import org.jbpm.services.api.service.ServiceRegistry;
import org.kie.api.task.TaskEvent;
import org.kie.api.task.TaskLifeCycleEventListener;
import org.kie.api.task.model.Task;

/**
 * TaskListener
 */
public class TaskListener implements TaskLifeCycleEventListener {

    @Override
    public void afterTaskCompletedEvent(final TaskEvent event) {
        Task task = event.getTask();
        Map<String, Object> outputs = task.getTaskData()
                                          .getTaskOutputVariables();

        RuntimeDataService runtimeDataService = (RuntimeDataService) ServiceRegistry.get()
                                                                                    .service(
                                                                                            ServiceRegistry.RUNTIME_DATA_SERVICE);

        Long processId = runtimeDataService.getTaskById(task.getId())
                                           .getProcessInstanceId();

        ProcessInstanceDesc processInstanceDesc = runtimeDataService.getProcessInstanceById(processId);
        String correlationKey = processInstanceDesc.getCorrelationKey();
        CaseService caseService = (CaseService) ServiceRegistry.get()
                                                               .service(ServiceRegistry.CASE_SERVICE);
        CaseFileInstance caseFileInstance = caseService.getCaseFileInstance(correlationKey);
        caseFileInstance.getData();

        Map<String, Object> updated = new HashMap<>();
        for (Map.Entry<String, Object> entry : caseFileInstance.getData()
                                                               .entrySet()) {
            updated.put(entry.getKey(), outputs.get(entry.getKey()));
        }
        if (updated.size() > 1)
            caseService.addDataToCaseFile(correlationKey, updated);
    }

    // *************************************************
    // VOID IMPLEMENTATION
    @Override
    public void beforeTaskActivatedEvent(final TaskEvent event) {
    }

    @Override
    public void beforeTaskClaimedEvent(final TaskEvent event) {
    }

    @Override
    public void beforeTaskSkippedEvent(final TaskEvent event) {
    }

    @Override
    public void beforeTaskStartedEvent(final TaskEvent event) {
    }

    @Override
    public void beforeTaskStoppedEvent(final TaskEvent event) {
    }

    @Override
    public void beforeTaskCompletedEvent(final TaskEvent event) {
    }

    @Override
    public void beforeTaskFailedEvent(final TaskEvent event) {
    }

    @Override
    public void beforeTaskAddedEvent(final TaskEvent event) {
    }

    @Override
    public void beforeTaskExitedEvent(final TaskEvent event) {
    }

    @Override
    public void beforeTaskReleasedEvent(final TaskEvent event) {
    }

    @Override
    public void beforeTaskResumedEvent(final TaskEvent event) {
    }

    @Override
    public void beforeTaskSuspendedEvent(final TaskEvent event) {
    }

    @Override
    public void beforeTaskForwardedEvent(final TaskEvent event) {
    }

    @Override
    public void beforeTaskDelegatedEvent(final TaskEvent event) {
    }

    @Override
    public void beforeTaskNominatedEvent(final TaskEvent event) {
    }

    @Override
    public void afterTaskActivatedEvent(final TaskEvent event) {
    }

    @Override
    public void afterTaskClaimedEvent(final TaskEvent event) {
    }

    @Override
    public void afterTaskSkippedEvent(final TaskEvent event) {
    }

    @Override
    public void afterTaskStartedEvent(final TaskEvent event) {
    }

    @Override
    public void afterTaskStoppedEvent(final TaskEvent event) {
    }

    @Override
    public void afterTaskFailedEvent(final TaskEvent event) {
    }

    @Override
    public void afterTaskAddedEvent(final TaskEvent event) {
    }

    @Override
    public void afterTaskExitedEvent(final TaskEvent event) {
    }

    @Override
    public void afterTaskReleasedEvent(final TaskEvent event) {
    }

    @Override
    public void afterTaskResumedEvent(final TaskEvent event) {
    }

    @Override
    public void afterTaskSuspendedEvent(final TaskEvent event) {
    }

    @Override
    public void afterTaskForwardedEvent(final TaskEvent event) {
    }

    @Override
    public void afterTaskDelegatedEvent(final TaskEvent event) {
    }

    @Override
    public void afterTaskNominatedEvent(final TaskEvent event) {
    }

}